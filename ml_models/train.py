"""takes the latest annotated training data, fine-tunes and evalutes the chatbot model"""
import subprocess
from sagemaker.pytorch import PyTorch
from sagemaker.session import Session
from sklearn.model_selection import train_test_split



class Sagemaker:
    def __init__(self):
        pass


    def get_trained_models(self):
        """returns trained models available to deploy"""
        pass


    def gather_data(self):
        pass

    def train(self, training_selection, model_selectionn):
        """creates a training job given  a selection of 
           training data"""
        train, test = train_test_split(df)
        train.to_csv(TRAIN_PATH, index=False)
        test.to_csv(TEST_PATH, index=False)
        session = Session()
        inputs_train = session.upload_data(TRAIN_PATH)
        inputs_test = session.upload_data(TRAIN_PATH)
        estimator = PyTorch(
            entry_point="train_deploy.py",
            source_dir="code",
            role=role,
            framework_version="1.3.1",
            py_version="py3",
            train_instance_count=2,
            train_instance_type="ml.p3.2xlarge",
            hyperparameters={
                "epochs": 1,
                "num_labels": 2,
                "backend": "gloo",
            }
        )
        estimator.fit({"training": inputs_train, "testing": inputs_test})
        # hyperparamter tuning w/ sagemaker
        # get estimator accuracy, loss (chart preferable)
        # get new model path
        return model_results 

    def deploy(self, model_path):
        """deploys the model to eastic inferece"""
        model_torchScript = BertForSequenceClassification.from_pretrained("model/", torchscript=True)
        device = "cpu"
        for_jit_trace_input_ids = [0] * 64
        for_jit_trace_attention_masks = [0] * 64
        for_jit_trace_input = torch.tensor([for_jit_trace_input_ids])
        for_jit_trace_masks = torch.tensor([for_jit_trace_input_ids])

        traced_model = torch.jit.trace(
            model_torchScript, [for_jit_trace_input.to(device), for_jit_trace_masks.to(device)]
        )
        torch.jit.save(traced_model, "traced_bert.pt")

        subprocess.call(["tar", "-czvf", "traced_bert.tar.gz", "traced_bert.pt"])
        predictor = pytorch.deploy(
            initial_instance_count=1,
            instance_type="ml.m5.large",
            accelerator_type="ml.eia2.xlarge"
        )



