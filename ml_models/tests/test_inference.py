import os
import json
import pytest
import boto3
from ml_models.bert import ei_model
from ml_models.bert import model as main_model


CONTEXT = """
Your check-in is on 8/22/21 at 15:01:00
Your check-out is on 9/11/21 at 10:30:00
This listing's adresss is None
This listing's postal code is 18648 This listing can accomodate at most None adult(s) and None kid(s)
Dining area: Ad eligendi nisi iste accusamus delectus itaque quaerat quas accusantium, quod vero corporis ab voluptatum qui reiciendis expedita quia? Error id officia, maxime eligendi dignissimos quo unde voluptates optio cum repellendus debitis reprehenderit, molestiae corrupti tempora officia eum veniam, debitis illo ex architecto laborum assumenda autem incidunt a fuga, repudiandae nihil earum magni. Quos dolores accusamus nemo molestiae adipisci saepe facere, placeat suscipit commodi eveniet mollitia eaque illo soluta error esse, dolorum ipsa laudantium hic quis totam tempore, eum temporibus doloribus aut ab ipsam qui rerum ut pariatur non asperiores, totam rerum et velit officia enim provident dolor itaque eligendi fugit.
Parquet floor: Tenetur fuga labore quasi repudiandae quisquam quidem velit vel, nemo laboriosam ullam quibusdam in sunt, eligendi reiciendis quaerat ipsum eaque enim sit quas, quasi repellat totam quo mollitia esse reprehenderit ad ea dignissimos, deleniti earum quod. Consequuntur quisquam laborum illum quos asperiores ex hic deserunt adipisci, necessitatibus voluptatibus officiis autem quae pariatur consequuntur delectus quasi, voluptate iste asperiores tempore temporibus fugiat iure quas. Explicabo ratione iure ipsum harum necessitatibus, nemo dolorem qui dolore vel sint ratione totam quidem quaerat libero, nulla rem veniam ipsam quisquam ullam odit, ab amet incidunt consectetur quae eos?
Bike: Quos consequuntur soluta ea similique sunt, distinctio soluta quis tenetur, natus est iusto corrupti, modi neque beatae aliquam recusandae repellendus porro nam tempore sint? Expedita est doloremque, veniam exercitationem fuga id corrupti sit, minus adipisci molestias doloribus amet vero tenetur vitae illum, fugit iusto suscipit omnis odit deserunt sequi error rem magnam?
Room service: Saepe voluptas omnis sequi repudiandae, quasi nobis quis temporibus quidem cum culpa id omnis quos ab esse, eum quasi suscipit?
Reef: Nobis vitae dolorem quasi ullam delectus voluptas consequuntur hic explicabo magnam magni, minima reprehenderit cumque repudiandae perspiciatis doloremque sed, modi a consequuntur corporis saepe quod eveniet obcaecati ipsam soluta excepturi, dolor rem sint nam tempora saepe?
Smoking is strictly prohibited at this listing
We allow pets at this listing
Yes, this listing is still available for these dates
"""



QA_input = {
    'question': 'Hey is this place still available?',
    'context': CONTEXT
}
EMOTION_INPUT = {
    "inputs": "Hmm not really having a good time to be honest. The place has combwebs and the you didn't answer my calls"
}


@pytest.mark.skip
def test_infer_bert():
    model = main_model.model_fn('../bert/weights/')
    request_body = json.dumps(QA_input)
    data = model.input_fn(request_body, 'application/json')
    res = model.predict_fn(data, model)
    print("GOT RES", res)


@pytest.mark.skip
def test_infer_ei_bert():
    model = ei_model.model_fn('../bert/weights/')
    request_body = json.dumps(QA_input)
    data = ei_model.input_fn(request_body, 'application/json')
    res = ei_model.predict_fn(data, model)
    print("GOT RES", res)


def test_remote_infer_ei_bert():
    endpoint = 'enso-nlp'
    runtime = boto3.Session().client('sagemaker-runtime')
    payload = json.dumps(QA_input)
    response = runtime.invoke_endpoint(EndpointName=endpoint, ContentType='application/json', Body=payload)
    result = json.loads(response['Body'].read().decode())
    print('GOT RESPONSE', result)


def test_remote_infer_emotion():
    endpoint = 'enso-bert-emotion'
    runtime = boto3.Session().client('sagemaker-runtime')
    payload = json.dumps(EMOTION_INPUT)
    response = runtime.invoke_endpoint(EndpointName=endpoint, ContentType='application/json', Body=payload)
    result = json.loads(response['Body'].read().decode())
    print('GOT RESPONSE', result)
