from sagemaker.huggingface import HuggingFaceModel
import sagemaker

# Hub Model configuration. https://huggingface.co/models
hub = {
    'HF_MODEL_ID':'bhadresh-savani/distilbert-base-uncased-emotion',
    'HF_TASK':'text-classification'
}
role = 'arn:aws:iam::373856063116:role/service-role/AmazonSageMaker-ExecutionRole-20210204T193692'

# create Hugging Face Model Class
huggingface_model = HuggingFaceModel(
    transformers_version='4.6.1',
    pytorch_version='1.7.1',
    py_version='py36',
    env=hub,
    role=role,
)

# deploy model to SageMaker Inference
endpoint_name = 'enso-bert-emotion'
predictor = huggingface_model.deploy(
    initial_instance_count=1, # number of instances
    instance_type='ml.m5.xlarge', # ec2 instance type
    endpoint_name=endpoint_name,
)

print(predictor.predict({'inputs': "I like you. I love you"}))
