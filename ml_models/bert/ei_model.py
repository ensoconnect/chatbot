import sys
import logging
import os
import json
from collections.abc import Iterable
from typing import Dict, List, Optional, Tuple, Union

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

import numpy as np
from transformers import AutoModelForQuestionAnswering, AutoTokenizer
import torch

tokenizer = AutoTokenizer.from_pretrained("deepset/roberta-base-squad2")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def model_fn(model_dir):
    print('LOADED MODEL')
    loaded_model = torch.jit.load(os.path.join(model_dir, "traced_bert.pt"))
    return loaded_model.to(device)


def decode(
        start: np.ndarray, end: np.ndarray, topk: int, max_answer_len: int) -> Tuple:
    """Take the output of any :obj:`ModelForQuestionAnswering` and will 
       generate probabilities for each span to be the actual answer."""
    # Ensure we have batch axis
    if start.ndim == 1:
        start = start[None]
    if end.ndim == 1:
        end = end[None]
    # Compute the score of each tuple(start, end) to be the real answer
    outer = np.matmul(np.expand_dims(start, -1), np.expand_dims(end, 1))
    # Remove candidate with end < start and end - start > max_answer_len
    candidates = np.tril(np.triu(outer), max_answer_len - 1)
    #  Inspired by Chen & al. (https://github.com/facebookresearch/DrQA)
    scores_flat = candidates.flatten()
    if topk == 1:
        idx_sort = [np.argmax(scores_flat)]
    elif len(scores_flat) < topk:
        idx_sort = np.argsort(-scores_flat)
    else:
        idx = np.argpartition(-scores_flat, topk)[0:topk]
        idx_sort = idx[np.argsort(-scores_flat[idx])]

    starts, ends = np.unravel_index(idx_sort, candidates.shape)[1:]
    """
    desired_spans = np.isin(starts, undesired_tokens.nonzero()) & np.isin(
        ends, undesired_tokens.nonzero())
    starts = starts[desired_spans]
    ends = ends[desired_spans]
    """
    scores = candidates[0, starts, ends]
    return starts, ends, scores


def input_fn(input_data, content_type):
    print("GOT TO BEGINNING OF INPUT", input_data)
    input = json.loads(input_data)
    kwargs = {}
    kwargs.setdefault("padding", "longest")
    kwargs.setdefault("doc_stride", 128)
    kwargs.setdefault("max_seq_len", 384)
    kwargs.setdefault("max_question_len", 64)

    # Define the side we want to truncate / pad and the text/pair sorting
    encoded_inputs = tokenizer(
        text=input['question'],
        text_pair=input['context'],
        padding=kwargs["padding"],
        truncation="only_second",
        max_length=kwargs["max_seq_len"],
        stride=kwargs["doc_stride"],
        return_tensors="np",
    )
    encoded_inputs['context'] = input['context']
    print('GOT INPUT')
    return encoded_inputs


def predict_fn(encoded_inputs, model):
    kwargs = {}
    kwargs.setdefault("handle_impossible_answer", False)
    kwargs.setdefault("max_answer_len", 150)
    kwargs.setdefault("topk", 1)

    print('GOT TO PREFICT', encoded_inputs)
    with torch.no_grad():
        input_ids = encoded_inputs['input_ids']
        attention_mask = encoded_inputs['attention_mask']
        input_ids = torch.tensor(input_ids, device=device)
        attention_mask = torch.tensor(attention_mask, device=device)
        print("GOT TO NO GRAD")
        try:
            with torch.jit.optimized_execution(True, {"target_device": "eia:0"}):
                print('-------------- USING ELASTIC INFERENCE ---------------')
                start, end = model(input_ids, attention_mask)[:2]
        except TypeError:
            start, end = model(input_ids, attention_mask)[:2]
        start_, end_ = start.cpu().numpy()[0], end.cpu().numpy()[0]

    min_null_score = 1000000  # large and positive
    # Normalize logits and spans to retrieve the answer
    start_ = np.exp(start_ - np.log(np.sum(np.exp(start_), axis=-1, keepdims=True)))
    end_ = np.exp(end_ - np.log(np.sum(np.exp(end_), axis=-1, keepdims=True)))

    if kwargs["handle_impossible_answer"]:
        min_null_score = min(min_null_score, (start_[0] * end_[0]).item())

    # Mask CLS
    start_[0] = end_[0] = 0.0
    starts, ends, scores = decode(
        start_, end_, kwargs["topk"], kwargs["max_answer_len"]
    )

    # Convert the answer (tokens) back to the original text
    enc = encoded_inputs
    s = starts[0]
    e = ends[0]
    score = scores[0]
    return {
        "score": score.item(),
        "start": enc.word_to_chars(enc.token_to_word(s), sequence_index=1)[0],
        "end": enc.word_to_chars(enc.token_to_word(e), sequence_index=1)[1],
        "answer": encoded_inputs['context'][
            enc.word_to_chars(enc.token_to_word(s), sequence_index=1)[0]:
            enc.word_to_chars(enc.token_to_word(e), sequence_index=1)[1]
        ],
    }

