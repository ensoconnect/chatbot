from transformers import AutoModelForQuestionAnswering, AutoTokenizer


def save_model():
    model_name = "deepset/roberta-base-squad2"
    model = AutoModelForQuestionAnswering.from_pretrained(model_name)
    model.save_pretrained('./bert/weights')


save_model()
