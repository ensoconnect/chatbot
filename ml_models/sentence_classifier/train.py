import nltk
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import classification_report


def train():
    posts = nltk.corpus.nps_chat.xml_posts()

    posts_text = [post.text for post in posts]

    #divide train and test in 80 20
    train_text = posts_text[:int(len(posts_text)*0.8)]
    test_text = posts_text[int(len(posts_text)*0.2):]

    #Get TFIDF features
    vectorizer = TfidfVectorizer(ngram_range=(1,3), 
                                 min_df=0.001, 
                                 max_df=0.7, 
                                 analyzer='word')

    X_train = vectorizer.fit_transform(train_text)
    X_test = vectorizer.transform(test_text)
    with open('weights/vectorizer.pkl', 'wb') as f:
        pickle.dump(vectorizer, f)
    return

    y = [post.get('class') for post in posts]

    y_train = y[:int(len(posts_text)*0.8)]
    y_test = y[int(len(posts_text)*0.2):]

    # Fitting Gradient Boosting classifier to the Training set
    gb = GradientBoostingClassifier(n_estimators = 400, random_state=0)
    #Can be improved with Cross Validation

    gb.fit(X_train, y_train)

    predictions_rf = gb.predict(X_test)

    #Accuracy of 86% not bad
    print(classification_report(y_test, predictions_rf))

    # now you can save it to a file
    with open('weights/sentence_classifier.pkl', 'wb') as f:
        pickle.dump(gb, f)

    # and later you can load it
    with open('filename.pkl', 'rb') as f:
        clf = pickle.load(f)
        res = clf.predict(vectorizer.transform(['How many listings am I allowed to checkin at?']))
        print("GOT RES", res)


train()
