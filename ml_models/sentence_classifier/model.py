import pickle
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import classification_report


classes = ['Accept', 'Bye', 'Clarify', 'Continuer', 'Emotion', 'Emphasis',
           'Greet', 'Other', 'Reject', 'Statement', 'System', 'nAnswer',
           'whQuestion', 'yAnswer', 'ynQuestion']


with open('weights/sentence_classifier.pkl', 'rb') as f:
    clf = pickle.load(f)
    vectorizer = pickle.load(open('weights/vectorizer.pkl', 'rb'))
    res = clf.predict(vectorizer.transform(['How many listings am I allowed to checkin at?']))
    print("GOT RES", res)
