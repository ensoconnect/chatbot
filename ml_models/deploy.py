"""takes the latest annotated training data, fine-tunes and evalutes the
   chatbot model"""
import time
import subprocess
import torch
import sagemaker
from sagemaker.pytorch.model import PyTorchModel
from sagemaker.pytorch import PyTorch
from sklearn.model_selection import train_test_split
import bert.model


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
sagemaker_session = sagemaker.Session()
bucket = "enso-ml-models"
prefix = "bert"
role = 'arn:aws:iam::373856063116:role/service-role/AmazonSageMaker-ExecutionRole-20210204T193692'


def trace_model():
    """Convert a model to TorchScript and save"""
    model = bert.model.model_fn('bert/weights/', torchscript=True)
    max_len = 384
    for_jit_trace_input_ids = [0] * max_len
    for_jit_trace_input = torch.tensor([for_jit_trace_input_ids])
    for_jit_trace_masks = torch.tensor([for_jit_trace_input_ids])
    traced_model = torch.jit.trace(
        model, [for_jit_trace_input.to(device),
                for_jit_trace_masks.to(device)])
    torch.jit.save(traced_model, "traced_bert.pt")
    subprocess.call(["tar", "-czvf", "traced_bert.tar.gz", "traced_bert.pt"])



def deploy_ei_model():
    """Deploy a model to elastic inference"""
    instance_type = 'ml.m5.large'
    accelerator_type = 'ml.eia2.xlarge'
    model_path = 'bert'
    tar_path = model_path + '/weights/traced_bert.tar.gz'
    print('Uploading tarball to S3...')
    model_data = sagemaker_session.upload_data(
        path=tar_path, bucket=bucket, key_prefix=prefix)
    endpoint_name = 'enso-roberta-v2'
    pytorch = PyTorchModel(
        model_data=model_data,
        role=role,
        entry_point='ei_model.py',
        source_dir=model_path,
        framework_version='1.3.1',
        py_version='py3',
        sagemaker_session=sagemaker_session
    )
    print('Deploying Model...')
    predictor = pytorch.deploy(
        initial_instance_count=1,
        instance_type=instance_type,
        accelerator_type=accelerator_type,
        endpoint_name=endpoint_name,
        wait=True,
    )

deploy_ei_model()
