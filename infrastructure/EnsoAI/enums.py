from enum import Enum


class ContentTypeEnum(str, Enum):
    guidebook = 'guidebook'
    upsell = 'upsell'
    fee = 'fee'
    checkin_step = 'checkin_step'
    checkout_step = 'checkout_step'
    experience = 'experience'
    agreement = 'agreement'
    refine_chat = 'refine_chat'

    @property
    def content_format(self):
        if self == ContentTypeEnum.guidebook:
            return 'HTML'
        return 'plain text'


class PromptTemplateEnum(str, Enum):
    answer_message = 'answer_message'
    answer_question = 'answer_question'
    classify_message = 'classify_message'
    summarize_question = 'summarize_question'
    generate_content = 'generate_content'
    refine_answer = 'refine_answer'
    is_context_answerable = 'is_context_answerable'
    determine_answerable_context = 'determine_answerable_context'


class AgentTypeEnum(str, Enum):
    content_writer = 'content_writer'
    guest_concierge = 'guest_concierge'


class MessageTypeEnum(str, Enum):
    question = 'question'
    other = 'other'
