import logging
import os
import langchain
langchain.verbose = False
from langchain.chains.llm import LLMChain
from langchain.docstore.document import Document
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from termcolor import colored
from exceptions import NeedExtraContextException
from constants import PROMPT_TEMPLATES
from helpers import load_prompt, parse_llm_output, format_chat_history, format_onestep_chat_history, retry
from enums import PromptTemplateEnum, MessageTypeEnum



embeddings = OpenAIEmbeddings(
    deployment='Enso-Embedding',
    model='text-embedding-ada-002',
    chunk_size=1,
)

class EnsoLLMChain:
  def __init__(self, llm):
      self.llm = llm

  @retry(20, 2)
  def run(self, template_name: PromptTemplateEnum, *args, **kwargs):
    prompt_template = PROMPT_TEMPLATES[template_name]
    #self.chain = LLMChain(
    #    llm=self.llm,
    #    prompt=prompt_template
    #)
    verbose = kwargs.pop('verbose')
    try:
        formatted_prompt = prompt_template.format(*args, **kwargs)
        if verbose:
            print(f'RUNNING PROMPT', colored(formatted_prompt, 'cyan'))
        response = self.llm(formatted_prompt)
        #print("GOT PROMPT RESPONSE", str(formatted_prompt), response)
    except:
        print("GOT PROMPT", prompt_template, args, kwargs)
        print(f'FAILED PROMPT: {prompt_template.format(*args, **kwargs)}')
        logging.exception('PROMPT FAILED')
        return ''
    return response





class ContentWriterAgent:
    """Writes content for short-term rental hosts"""

    def __init__(
            self, llm
    ):
        self._llmchain = lambda prompt: LLMChain(llm=llm, prompt=load_prompt(prompt))

    @retry(20, 2)
    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)

    def run(self, content_type, prompt, context_articles):
        #context = self.fetch_context(context_articles, prompt)
        content = self.generate_content(content_type, prompt, context_articles)
        return content

    def generate_content(self, content_type, prompt, context):
        """Writes a piece of content given prompt, content_type and context"""
        llm = self._llmchain(PromptTemplateEnum.generate_content)
        llm_output = llm.run(
            content_type=content_type,
            content_type_format=content_type.content_format,
            input=prompt.lower(),
            context=context
        ).split('You have access to the following')[0]
        return parse_llm_output(llm_output)

    def fetch_context(self, context, prompt):
        """Fetches data relevant to the question"""
        docs = [Document(page_content=f'{title}: {c}') for title, c in context.items()]
        db = FAISS.from_documents(docs, embeddings)
        docs = db.similarity_search(prompt)
        # print("GOT DOCS", docs)
        # print("QUERIED CONTEXT", chat_history[-1]['message'],)
        # self._log("got context", '\n'.join(d.page_content for d in docs[:2]))
        return '\n\n'.join(d.page_content for d in docs[:5])


class GuestConciergeAgent:
    def __init__(
            self, llm, verbose=False
    ):
        self.chain = EnsoLLMChain(llm)
        self._llmchain = lambda prompt: LLMChain(llm=llm, prompt=load_prompt(prompt))
        self._llm = llm
        self.verbose = verbose

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)

    def run(self, chat_history, context_articles):
        message_type = self.classify_message(chat_history)
        response = {'properties': {}}

        try:
            if message_type == MessageTypeEnum.question:
                context, summary = self.fetch_context(chat_history, context_articles)
                response['answer'] = self.answer_message(chat_history, context)
                response['properties']['summary'] = summary
            else:
                context, summary = self.fetch_context(chat_history, context_articles)
                response['answer'] = self.answer_message(chat_history, context)
                response['properties']['summary'] = summary
        except NeedExtraContextException as e:
            response['properties']['need_context'] = e.extra_context

        return response


    @retry(20, 2)
    def classify_message(self, chat_history) -> MessageTypeEnum:
        """Classifies a message"""
        formatted_chat_history = format_chat_history(chat_history)
        llm = self._llmchain(PromptTemplateEnum.classify_message)
        llm_output = llm(formatted_chat_history)['text']

        def _parse_response():
            for char in llm_output:
                if char == '1':
                    return MessageTypeEnum.question
            return MessageTypeEnum.other

        response = _parse_response()
        self._log("Classified message", response)
        return response

    def answer_message(self, chat_history, context):
        """Answers the guest's latest message"""

        # determine whether is answerable
        is_answerable_string = self.chain.run(
            PromptTemplateEnum.is_context_answerable,
            input=format_onestep_chat_history(chat_history),
            context=context,
            verbose=self.verbose
        ).lower()
        self._log("determined whether is answerable", is_answerable_string)
        yes_index = is_answerable_string.find('yes')
        no_index = is_answerable_string.find('no')
        if yes_index != -1:
            if no_index != -1:
                is_answerable = yes_index < no_index
            else:
                is_answerable = True
        else:
            is_answerable = False

        # if not, fetch needed context & raise exception
        if not is_answerable:
            extra_context = self.chain.run(
                PromptTemplateEnum.determine_answerable_context,
                input=format_onestep_chat_history(chat_history),
                context=context,
                verbose=self.verbose
            )
            self._log("determined extra needed context", extra_context)
            extra_context = extra_context.split('#')[0].strip()
            raise NeedExtraContextException(extra_context)

        # answer question
        llm_output = self.chain.run(
            PromptTemplateEnum.answer_question,
            input=format_onestep_chat_history(chat_history),
            context=context,
            verbose=self.verbose
        ).split('<END_OF_TURN>')[0]
        llm_output = llm_output.strip()
        self._log("answered message", llm_output)

        # re-phrase question politely
        llm_output = self.chain.run(
            PromptTemplateEnum.refine_answer,
            input=format_onestep_chat_history(chat_history),
            context=llm_output,
            verbose=self.verbose
        )
        if 'Host:' in llm_output:
            llm_output = llm_output.split('Host:')[1]
        print("GOT RAW REFINED ANSWER", llm_output)
        llm_output = llm_output.split('<END_OF_TURN>')[0].strip()
        self._log("refined message", llm_output)

        return llm_output

    #@retry(20, 2)
    def fetch_context(self, chat_history, context):
        """Fetches data relevant to the question"""
        context_query = self.chain.run(
            PromptTemplateEnum.summarize_question,
            input=format_onestep_chat_history(chat_history),
            verbose=self.verbose
        ).strip().split('\n')[0]
        self._log("refined message", context_query)

        docs = [Document(page_content=f'{title}: {c}') for title, c in context.items()]
        print("START EMBED")
        db = FAISS.from_documents(docs, embeddings)
        print("FINISH EMBED")
        #context_query = chat_history[-1]['message']
        docs = db.similarity_search(context_query)
        #print(f"QUERYING CONTEXT BY {context_query}")
        self._log("got context", '\n\n'.join(d.page_content for d in docs[:5]))
        return '\n\n'.join(d.page_content for d in docs), context_query

    def _log(self, step, output):
        delim = "=" * 20
        if not self.verbose:
            return
        print(colored(f"{delim} THINKING: {step} {delim}", "light_yellow"))
        print(f"OUTPUT: {output}")
        print(colored(f'{delim} END OF THOUGHT {delim}', "light_yellow"))
