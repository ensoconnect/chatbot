


class NeedExtraContextException(Exception):
    def __init__(self, extra_context):
        self.extra_context = extra_context
        super().__init__('Cannot answer: Need extra context')