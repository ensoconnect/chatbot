from langchain.prompts import PromptTemplate
import time
import signal
from constants import PROMPT_TEMPLATES


def load_prompt(template_name: str) -> PromptTemplate:
    return PROMPT_TEMPLATES[template_name]


def format_chat_history(chat_history):
    formatted_chat_history = [
        f"{'Guest' if message['sender_type'] == 'other_user' else 'Host'}: {message['message']}"
        for message in chat_history[-5:]
    ]
    return '\n'.join(formatted_chat_history)


def format_onestep_chat_history(chat_history):
    formatted_chat_history = [
        f"{'Guest' if message['sender_type'] == 'other_user' else 'Host'}: {message['message']} <END_OF_TURN>"
        for message in chat_history[-5:]
    ]
    return '\n'.join(formatted_chat_history)


def parse_llm_output(output):
    """generic parse that strips out any irrelevant text from LLM outputs"""
    for t in ['<|im_sep|>', '<|im_end|>']:
        output = output.replace(t, '')
    return output.strip()


class TimeoutException(Exception):
    pass


def timeout(seconds_before_timeout):
    def decorate(f):
        def handler(signum, frame):
            raise TimeoutException

        def new_f(*args, **kwargs):
            old = signal.signal(signal.SIGALRM, handler)
            old_time_left = signal.alarm(seconds_before_timeout)
            if (
                    0 < old_time_left < seconds_before_timeout
            ):
                signal.alarm(old_time_left)
            start_time = time.time()
            try:
                result = f(*args, **kwargs)
            finally:
                if old_time_left > 0:
                    old_time_left -= time.time() - start_time
                signal.signal(signal.SIGALRM, old)
                signal.alarm(old_time_left)
            return result

        new_f.__name__ = f.__name__
        return new_f

    return decorate


def retry(seconds_before_timeout: int, num_retries: int):
    def decorate(func):
        def wrapper(*args, **kwargs):
            tries_left = num_retries
            while tries_left > 0:
                try:
                    return timeout(seconds_before_timeout)(func)(*args, **kwargs)
                except TimeoutException:
                    print("TIMED OUT...retrying")
                    tries_left -= 1

        return wrapper

    return decorate
