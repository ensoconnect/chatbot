import json
from infrastructure.EnsoAI.chatbot_trainer.extra_context import EXTRA_CONTEXT
from infrastructure.EnsoAI.agents import GuestConciergeAgent
from infrastructure.EnsoAI.handler import llm
from termcolor import colored



PREFIX = '../../../backend/scripts/chatbot_datagen/'


def load_examples(enso_key):
  with open(PREFIX + f"chatbot_validation_dataset/{enso_key}_dataset.json") as file:
    contents = json.load(file)
  return contents


def write_examples(enso_key, examples):
  with open(PREFIX + f"chatbot_validation_dataset/{enso_key}_dataset.json", 'w') as file:
    file.write(json.dumps(examples, indent=4))


def load_example(example_id, example):
  with open(PREFIX + f"chatbot_validation_dataset/context/{example['context_version']}/{example_id}.json") as file:
    contents = json.load(file)
  return contents


def call_agent(example_id, example, verbose=False):
  example_data = load_example(example_id, example)
  # print(json.dumps(example_data['context'], indent=4))
  question = example_data['chat_history'][-1]['message']
  print(colored(f"Generating response to {question}...", 'red'))
  for k in ['Booking CHECK-OUT', 'Booking CHECK-IN']:
    example_data.pop(k, None)

  context = {
    **EXTRA_CONTEXT,
    **example_data['context']
  }
  answer = GuestConciergeAgent(llm, verbose=verbose)(example_data['chat_history'], context)
  print("Finished Generating response...")
  return answer