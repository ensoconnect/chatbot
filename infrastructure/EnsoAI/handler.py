from langchain.llms import AzureOpenAI

from enums import AgentTypeEnum, ContentTypeEnum
from agents import ContentWriterAgent, GuestConciergeAgent


llm = AzureOpenAI(
    deployment_name="Enso-GPT3",
    model_name="gpt-35-turbo",
    temperature=0
)


def handler(event, _):
    agent_type = AgentTypeEnum(event['agent_type'])
    context_articles = event['context_articles']

    if agent_type == AgentTypeEnum.content_writer:
        content_type = ContentTypeEnum(event['content_type'])
        return ContentWriterAgent(llm)(content_type, event['prompt'], context_articles['input'])
    if agent_type == AgentTypeEnum.guest_concierge:
        return GuestConciergeAgent(llm)(event['chat_history'], context_articles)
