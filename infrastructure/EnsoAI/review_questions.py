import sys


sys.path.append('../../')
from infrastructure.EnsoAI.enums import ContentTypeEnum
from infrastructure.EnsoAI.chatbot_trainer.single_example import SINGLE_CHAT_HISTORY, SINGLE_CONTEXT
from infrastructure.EnsoAI.agents import GuestConciergeAgent, ContentWriterAgent
from infrastructure.EnsoAI.handler import llm
from infrastructure.EnsoAI.chatbot_trainer.example_data import load_examples, write_examples, call_agent
import questionary
from termcolor import colored


AGENT_VERSION = 'v1'


def rate_question(question, answer, host_answer):
  print(colored(question, "light_yellow"), "\n")
  print("CHATBOT ANSWER", colored(answer, "light_green"), "\n")
  print("HOST ANSWER", colored(host_answer, "light_green"), "\n")

  response = questionary.confirm(f'Answer correct?').ask()
  print("GOT CONFIRM RESPONSE", response)
  return response


def generate_responses():
  enso_key = questionary.text('enter enso_key:').ask()
  examples = load_examples(enso_key)
  try:
    for example_id, example in examples.items():
      if answer := example['answers'].get(AGENT_VERSION):
        continue
      example['answers'][AGENT_VERSION] = {
        'answer': call_agent(example_id, example)
      }
  except KeyboardInterrupt:
    pass
  write_examples(enso_key, examples)


def regenerate_responses():
  enso_key = questionary.text('enter enso_key:').ask()
  examples = load_examples(enso_key)
  try:
    for example_id, example in examples.items():
      if example['answers'].get(AGENT_VERSION, {}).get('rating') is not False:
        continue
      answer = call_agent(example_id, example, verbose=True)
      example['answers'][AGENT_VERSION] = {
        'answer': answer,
        'rating': rate_question(example['question'], answer, example['host_answer'])
      }
  except KeyboardInterrupt:
    pass
  write_examples(enso_key, examples)


def rate_answers():
  enso_key = questionary.text('enter enso_key:').ask()
  examples = load_examples(enso_key)
  try:
    for example_id, example in examples.items():
      if not example.get('answers'):
        continue
      answer_agent_version, answer = list(example['answers'].items())[-1]
      if answer.get('rating'):
        continue
      example['answers'][answer_agent_version]['rating'] = rate_question(example['question'], answer['answer'], example['host_answer'])
  except KeyboardInterrupt:
    pass
  write_examples(enso_key, examples)


prompt = """You are an airbnb host answering your guests' messages. Your job is to summarize the guest's latest question from the below conversation history.

## start of conversation history ##
Host: Hi there how is your stay going so far? <END_OF_TURN>
Guest: Hi there my family and I wanna book. Is this place still available? <END_OF_TURN>
## end of conversation history ##

Summarize what the guest's latest question is asking about. Provide a 1-2 sentence answer:"""

def respond_once():
    #print(ContentWriterAgent(llm)(ContentTypeEnum.refine_chat, 'Translate to french', prompt))
    #print("RUNNING PROMPT", prompt)
    #print("GOT INITIAL REFINE ANSWER", llm(prompt))
    #return
    answer = GuestConciergeAgent(llm, verbose=True)(SINGLE_CHAT_HISTORY, SINGLE_CONTEXT)
    print("GOT ANSWER", answer)


if __name__ == '__main__':
    steps = {
      'Run single test response': respond_once,
      'Generate responses to unanswered answered questions': generate_responses,
      'Re-generate responses to incorrectly answered questions': regenerate_responses,
      'Rate the accuracy of answers': rate_answers
    }
    selected_step = questionary.select(
      'select step to run', list(steps.keys())
    ).ask()
    steps[selected_step]()