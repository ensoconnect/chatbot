"""utility functions for api gateway requests & responses"""
import os
import logging
import traceback
import simplejson
from pydantic import ValidationError

logger = logging.getLogger()
logger.setLevel(logging.INFO)
local = os.environ.get('environment')


def optional_arg_decorator(fn):
    def wrapped_decorator(*args, **kwargs):
        if not kwargs:
            return fn(args[0])
        else:
            def real_decorator(decoratee):
                return fn(decoratee, **kwargs)

            return real_decorator

    return wrapped_decorator


@optional_arg_decorator
def API_handler(func, response_type='text/plain'):
    """this function wraps any Lambda handler function 
       that receives incoming API calls"""
    
    def log_error(msg, event, level='warning'):
        """log an API error"""
        logfunc = {
            'warning': logger.warning,
            'error': logger.error
        }[level]
        logfunc(f"Request: {event['body']}")
        logfunc(traceback.format_exc())
        logfunc(msg)

    def API_response(status_code, body):
        if response_type == 'text/plain':
            body = simplejson.dumps(body)
        return {
            'statusCode': status_code,
            'headers': {
                'Content-Type': response_type,
                'Access-Control-Allow-Origin': '*'
            },
            'body': body
        }

    def wrap_handler(*args):
        """Wrap API Gateway request"""
        event, context = args
        path = event['resource'][1:].split('/')
        method = event['httpMethod']
        has_query = method in ['GET', 'DELETE']
        if has_query:
            body = event['queryStringParameters']
        else:
            body = event['body']
            request_type = event['headers'].get('Content-Type', 'application/json')
            if request_type.startswith('application/x-www-form-urlencoded'):
                pass
            elif isinstance(event['body'], str):
                body = simplejson.loads(event['body'])

        try:
            response = func(path, method, body)
        except ValidationError as e:
            log_error(f'User Validation Error: {e.errors()}', event)
            return API_response(400, {'error': e.errors()})
        except:
            if local:
                raise
            log_error('UNKNOWN APPLICATION ERROR', event, 'error')
            return API_response(502, {'error': 'Unknown error, sorry for the inconvenience'})

        return API_response(200, response)

    return wrap_handler

