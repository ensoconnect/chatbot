import json
import boto3
from infrastructure.IngestData.data.training_data import TrainingData

sqs = boto3.resource("sqs")
queue = sqs.get_queue_by_name(QueueName="enso-training-queue")


def handler(_, __):
    """handles incoming apigw calls"""
    out = {
        'queue': [],
        'unhandled_queue': []
    }

    while True:
        messages = queue.receive_messages(MaxNumberOfMessages=10)
        if not messages:
            break
        for message in messages:
            body = json.loads(message.body)
            source = body['source']
            out[source].append(body)
            message.delete()

    tr = TrainingData()
    for source, data in out.items():
        if not data:
            print(f'NO DATA FOR SOURCE {source}')
            continue
        tr.upload_batch(data, source=source)
        print(f"***** UPLOADED {len(data)} MESSAGES TO {source} *****")


