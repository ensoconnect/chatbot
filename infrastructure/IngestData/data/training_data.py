from typing import Literal
import boto3
import json


BATCH_SIZE = 100
BUCKET = 'enso-training-data'


class TrainingData:
    def __init__(self):
        self.s3 = boto3.client('s3')

    def load_trained(self, start=0):
        files = self.s3.list_objects(
            Bucket=BUCKET, Prefix='trained/').get('Contents')
        if files:
            return json.loads(self.s3.get_object(
                Bucket=BUCKET, Key=files[-(start+1)]['Key'])['Body'].read())
        return []

    def load_queue(self, source: Literal['queue', 'unhandled_queue'] = 'queue'):
        files = self.s3.list_objects(
            Bucket=BUCKET, Prefix=f'{source}/').get('Contents')
        if not files:
            return {'complete': [], 'incomplete': [], 'batch': 0}
        last_file = files[-1]['Key']
        return json.loads(self.s3.get_object(
            Bucket=BUCKET, Key=last_file)['Body'].read())

    def save(self, complete, incomplete, batch, source='queue'):
        if incomplete:
            body = {'complete': complete, 'incomplete': incomplete, 'batch': batch}
            data = bytes(json.dumps(body).encode('UTF-8'))
            self.s3.put_object(Bucket=BUCKET, Key=f'{source}/BATCH_{batch}.json', Body=data)
        else:
            data = bytes(json.dumps(complete).encode('UTF-8'))
            tr_files = self.s3.list_objects(
                Bucket=BUCKET, Prefix='trained/').get('Contents')
            if tr_files:
                tr_file = tr_files[-1]['Key']
                tr_batch = int(tr_file.split('BATCH_')[1].split('.json')[0]) + 1
            else:
                tr_batch = 0

            self.s3.put_object(
                Bucket=BUCKET, Key=f'trained/BATCH_{tr_batch}.json', Body=data)
            self.s3.delete_object(
                Bucket=BUCKET, Key=f'{source}/BATCH_{batch}.json')
        return True

    def upload_batch(self, messages, source='queue'):
        """uploads a batch of training data to s3"""
        data = self.load_queue(source)
        for data_chunk in self.chunks(messages, 50):
            if len(data['incomplete']) > 50:
                data = {'complete': [], 'incomplete': [], 'batch': data['batch'] + 1}
            data['incomplete'] += data_chunk

            self.s3.put_object(
                Bucket=BUCKET, Key=f"{source}/BATCH_{data['batch']}.json",
                Body=bytes(json.dumps(data).encode('UTF-8'))
            )

    def get_batch(self):
        pass

    def generate_from_messages(self):
        """generates training data from random message threads"""
        pass

    @staticmethod
    def chunks(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]
