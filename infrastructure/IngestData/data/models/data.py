from enum import Enum
from typing import List, Union, Optional, Literal
from pydantic import BaseModel
from ensoapi.models import MessageChannelTypeEnum, MessageSenderTypeEnum


class ChatbotConversationMessageModel(BaseModel):
    id: str
    message: str
    channel: MessageChannelTypeEnum
    by: Literal["guest", "chatbot"]


class ChatbotResponseModel(BaseModel):
    id: str
    guest_id: str
    booking_id: str
    message: str
    enso_user_id: Optional[str]
    sent: int
    channel: MessageChannelTypeEnum
    sender_type: MessageSenderTypeEnum


class MiniMessage(BaseModel):
    user: int
    msg: str


class TrainingData(BaseModel):
    msg_snippet: List[MiniMessage]
    knowledge_base: str
    answer: Optional[List[int]]
    human_answer: str
    kb_update: Optional[str]


class UnclassifiedData(BaseModel):
    msg_snippet: List[MiniMessage]
    knowledge_base: str
    answer: List[int]
    ks_objects: List[dict]


class ChatbotContextObjectModel(BaseModel):
    pass
