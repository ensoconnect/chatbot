from enum import Enum
from typing import List, Union, Optional, Literal
from pydantic import BaseModel
from ensoapi.models import MessageChannelTypeEnum, MessageSenderTypeEnum


class CorrectionEnum(str, Enum):
    kb_update = 'kb_update'
    custom_kb_update = 'custom_kb_update'
    answer_update = 'answer_update'
    question_update = 'question_update'
