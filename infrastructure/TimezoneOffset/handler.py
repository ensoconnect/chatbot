from timezonefinder import TimezoneFinder
import pytz
import datetime

timezone_finder = TimezoneFinder()


def handler(event, _):
    lat = float(event['lat'])
    lng = float(event['lng'])
    timezone_name = timezone_finder.timezone_at(lat=lat, lng=lng)
    offset = datetime.datetime.now(pytz.timezone(timezone_name)).strftime('%z')
    offset_hours = offset[:3]
    offset_minutes = str(int(offset[3:]) / 60)[1:]
    return {'offset': offset_hours + offset_minutes, 'timezone_name': timezone_name}