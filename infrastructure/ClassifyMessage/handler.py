import json
import sklearn
import pickle

with open('weights/sentence_classifier.pkl', 'rb') as f:
    clf = pickle.load(f)
with open('weights/vectorizer.pkl', 'rb') as f:
    vectorizer = pickle.load(f)

GREET_TOKENS = ['hello there', 'hello', 'hi there', 'hi', 'hey there', 'hey']

def handler(event, _):
    split_msg = event['sentences']
    msg_types = ''
    sentences = []
    for sentence in split_msg:
        l_sent = sentence.lower()
        for token in GREET_TOKENS:
            if token in l_sent:
                sentence = sentence.replace(token, '')

        if l_sent.endswith('?'):
            classification = 'whQuestion'
        else:
            classification = clf.predict(vectorizer.transform([sentence]))[0]
        msg_types += classification

        sentences.append({'message_type': classification, 'message': sentence})
    return {'sentences': sentences, 'message_type': msg_types}







