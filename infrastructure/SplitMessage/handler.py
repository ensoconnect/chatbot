import json
import spacy

def set_custom_boundaries(doc):
    for token in doc[:-1]:
        if token.text == ".(" or token.text == ").":
            doc[token.i + 1].is_sent_start = True
        elif token.text == "Rs." or token.text == ")":
            doc[token.i + 1].is_sent_start = False
    return doc


def handler(event, _):
    nlp = spacy.load("/opt/en_core_web_sm-2.2.5/en_core_web_sm/en_core_web_sm-2.2.5")
    nlp.add_pipe(set_custom_boundaries, before="parser")
    return {
        'sentences': [str(sent) for sent in nlp(event['message']).sents]
    }







