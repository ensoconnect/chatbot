import boto3
from weasyprint import HTML


client = boto3.client('s3')


def handler(event, _):
    html = event['html']
    filename = event['filename']
    bucket = event['bucket']

    client.put_object(
        Bucket=bucket,
        Body=HTML(string=html).write_pdf(),
        Key=filename,
        ContentType='application/pdf'
    )

    return {
        'url': f'https://{bucket}.s3.amazonaws.com/{filename}'
    }