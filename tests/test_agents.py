import pytest
from langchain.llms import AzureOpenAI
from infrastructure.EnsoAI.agents import ContentWriterAgent, GuestConciergeAgent
from infrastructure.EnsoAI.enums import ContentTypeEnum


llm = AzureOpenAI(
    deployment_name="Enso-GPT3",
    model_name="gpt-35-turbo",
    temperature=0
)


context_articles = {
  "Guest stay details: Checkin & checkout info": """
Checkin date: July 10th, 2023
Checkin time: 4:00 PM
Checkout date: July 14th, 2023
Checkout time: 12:00 PM
""",
  "Listing Guidebooks: A/C Location & Instructions, Parking Information": """
A/C Information
You can find the A/C in the closet next to the entrance. Simply turn the dial to change the temperature

Heating Information
You can find the heating next to the kitchen sink.

Parking Information
You can find the parking garage next to the 'P' sign near the main entrance.
""",
  "Listing Fee: Safety Deposit": "",
  "Listing Fee: Damage Waiver": "",
  "Listing address": """
Listing address: 5 Fernalroy Blvd, Toronto ON M8Z 3V5
Listing city: Toronto
Listing state: Ontario
Listing country: Canada
  """
}

chat_history=[
  {'sender_type': 'other_user', 'message': "hi I forgot what's my checkin date again?"},
  {'sender_type': 'user', 'message': "Hi Shaun, your checkin date is May 12th, 2023"},
  {'sender_type': 'other_user', 'message': "Ok great and what time do I show up?"},
]

event = {
    'agent_type': 'content_writer',
    'content_type': 'guidebook',
    'prompt': 'Write me a guidebook about the garbage policy. Garbage is collected every tuesday',
    'context': context_articles
}

import json
print(json.dumps(event))


class TestContentWriterAgent:
    @pytest.fixture(autouse=True)
    def _setup(self):
        self.agent = ContentWriterAgent(llm)

    def test_ContentWriterAgent_call(self):
        response = self.agent(ContentTypeEnum.guidebook, "Write me a guidebook about the garbage policy. Garbage is collected every tuesday", context_articles)
        assert isinstance(response, str)


class TestGuestConciergeAgent:
    @pytest.fixture(autouse=True)
    def _setup(self):
        self.agent = GuestConciergeAgent(llm, verbose=True)

    def test_GuestConciergeAgent_call(self):
        response = self.agent(chat_history, context_articles)
        assert isinstance(response, str)